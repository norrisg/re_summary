# RECURSIVE ESTIMATION SUMMARY

[**CHECK FOR LATEST RELEASE**](https://gitlab.ethz.ch/norrisg/re_summary/-/releases/permalink/latest)

## CHANGELOG

### Changelog - 2023.02.20 `v1.2.0`

- **Updated for public consumption**
- moved sections to own file
- changed font
- added git and email links

### Changelog - 2022.08.17 `v1.1.0`

- Many small updates and optimizations to make exam ready

### Changelog - 2022.07.20 `v1.0.0`

- Finished `OBSERVER BASED CONTROL` section, this marks end of lecture content

### Changelog - 2022.07.19 `v0.11.0`

- Reformatted and completed `PF` section
- Small fixes and compactifications of older sections

### Changelog - 2022.07.18 `v0.10.0`

- Reformatted and completed `EKF` section

### Changelog - 2022.07.15 `v0.9.0`

- Updated L08 content `ASYMPT. PROP. OF KF`, `DETECT. / STABIL.`, `STEADY-STATE KF`
- Added .pdf to repo once again, gitlab does not properly support adding pdfs to releases >:|

### Changelog - 2022.07.14 `v0.8.0`

- Updated `PROBABILITY` section to use tables for generic statistics definitions and properties to save space
- Updated existing material to use more `\begin{highlightbox}` to incorporate both text and math into highlighted boxes
- Completed `DISTRIBUTION SAMPLING`
- Completed `CHANGE OF VARIABLES` (notably for multivariables case, still has superflous information)
- Tweaked `BAYESIAN TRACKING` section
- Finished `ESTIMATE EXTRACTION`
  - Tweaked `ML` and `MAP` subsections
  - Fleshed out `WLS`, `RLS` sections
- Continued work on `KALMAN FILTER`
  - Tweaked `AUXILLIARY VARIABLES` subsections
  - Tweaked `KALMAN FILTER` subsections

- Minor adjustments to class file
  - Fixed spacing for `whitebox` environment
  - Began attempting to replace `\mathbox` command with an environment, `empheq` package does not support this directly, but rather uses special syntax, needs further work.
  - Updated font option

***

### Changelog - 2022.04.27

- `norrisgsummary.cls` changes taken from `MPC Summary`
  - minor changes to `b/w` color option:
      \colorlet{color3}{black!15} -> \colorlet{color3}{black!20}
  - Added new `tcolorbox` environment: `highlightbox`
    - Intended to highlight mixed text/math as in theorems or statements
  - Modified existing `yellowbox` and `bluebox` to contain several qol updates
- Replaced manual `tcolorboxes` with `highlightboxes`
- Added `EKF -- EXTENDED KALMAN FILTER` section for lecture 9
  - Initial implementation of `EKF -- DT EQUATIONS`, requires description of why we must extend KF 
  - Initial implementation of `HYBRID EKF`, also requires explanation of what has changed about the model

***

### Changelog - 2022.04.21

- Continued work on Lecture 08 material
  - Fleshed out `ASYMPT. PROPERTIES OF KF` **still needs some work**
  - Fleshed out `DETECTABILITY/STABILIZABILITY`
  - Fleshed out `STEADY-STATE KF` **still needs some work**

***

### Changelog - 2022.04.20

- Implemented material from Lecture 06 in new `KALMAN FILTER` section
  - `MODEL` subsection is potentially unnecessary or should be consolidated into one big model definition in the beginning?
  - Wrote `GRV` subsection
  - Wrote `AUX. VAR.` subsection. **still needs some work**
- Implemented Lecture 07 material
  - Wrote initial versions of `KALMAN FILTER EQUATIONS` and `KALMAN FILTER IMPLEMENTATION` subsections. 
- Outlined material from Lecture 08 `ASYMPT. PROP. OF KF`, `DETECTABILITY/STABILIZABILITY`, `STEADY-STATE KF` subsections

***

### Changelog - 2022.03.23

- Added `COMPUTER IMPLEMENTATION` to `BAYESIAN TRACKING` section
- Began `ESTIMATE EXTRACTION`

***

### Changelog - 2022.03.22

- Split `EXPECTATION` into own whitebox
- Added `Law of unconcious statistician`
- Added `BAYESIAN TRACKING` section
  - Wrote **initial** versions of `BAYES THEOREM` and `BAYESIAN TRACKING` formulas

***

### Changelog - 2022.03.04

- Added DRV and CRV sections

***

### Changelog - 2022.03.01

- Initial commit
