\section{OBSERVER-BASED CONTROL}

\begin{whitebox}{\textbf{LTI OBSERVER}}

    \begin{minipage}[c]{0.3\linewidth}
        \blue{LTI System}
        \begin{itemize}[leftmargin=1em]
            \item $v, w$ 0-mean noise CRVs
        \end{itemize}
    \end{minipage}
    \begin{minipage}[c]{0.65\linewidth}
        \mathbox{
            x(k) &= A x(\kminone) + B u(\kminone) + v(\kminone) \\
            z(k) &= H x(k) + w(k)
        }
    \end{minipage}

    \begin{itemize}[leftmargin=1em]
        \item $\mathbb{E}\{\widehat{x}(k)\} \xrightarrow{k\to\infty} \mathbb{E}\{x(k)\}$, $\mathrm{Var}\{\widehat{x}(k)\}$ bounded
    \end{itemize}

    \blue{Luenberger Observer} same structure as steady-state KF
    \mathboxplus{
        \widehat{x}(k) &= A \widehat{x}(\kminone) + B u (\kminone) + K (\bar{z}(k) - \widehat{z}(k)) \\
        &= ({\scriptstyle \mathbb{I} - K_\infty H}) A \widehat{x}(\kminone) + ({\scriptstyle \mathbb{I} - K_\infty H})Bu(\kminone) + K_\infty \bar{z}(k) \\
        \widehat{z}(k) &= H(A \widehat{x}(\kminone) + Bu(\kminone))
    }

    \begin{minipage}[c]{0.45\linewidth}
        \blue{Error} $e(k) = x(k) - \widehat{x}(k)$ \\
        {\footnotesize in absence of noise $\bar{z}(k) = z(k)$}
    \end{minipage}
    \begin{minipage}[c]{0.50\linewidth}
        \mathbox{
            e(k) = (\mathbb{I}-KH)A e(\kminone)
        }
    \end{minipage}
    
    
    \begin{itemize}[leftmargin=1em]
        \item $(\mathbb{I}-KH)A$ must be stable (all $\lvert\lambda\rvert < 1$) for $e\xrightarrow{k\to\infty}0$
        \item Such $K$ exists iff $(A,H,A)$ detectable $\Leftrightarrow$ $(A,H)$ detectable
    \end{itemize}

    \blue{Observer Design} If $(A,H)$ detectable, construct $K$
    \begin{whitebox}{}
        \footnotesize
        \begin{description}
            \item[Pole Placement:] place $\lambda$ of $e$-dyn. of observ. modes at desired locations 
            \item[Steady State KF:] Yields optimal $K$ (min steady-state MSE) given statistics of $v,w$. 
            Also need $(A,G)$ stabilizable, $\mathrm{Var}\{v(k)\} = GG^\top$
        \end{description}
    \end{whitebox}

    \begin{minipage}[c]{0.25\linewidth}
        \blue{Alternative Formulation}
    \end{minipage}
    \begin{minipage}[c]{0.70\linewidth}
        \footnotesize
        \mathbox{
            \widehat{x}(k+1) &= A\widehat{x}(k) + Bu(k) + K(\bar{z}(k) - \widehat{z}(k))\\
            \widehat{z}(k) &= H\widehat{x}(k) \leadsto \mathrm{ delayed measurement}
        }
    \end{minipage}

    \begin{minipage}[c]{0.25\linewidth}
        \blue{Error dynamics}
    \end{minipage}
    \begin{minipage}[c]{0.70\linewidth}
        \footnotesize
        \mathbox{
            e(k+1) = (A-KH)e(k)
        }
    \end{minipage}

\end{whitebox}

\begin{whitebox}{\textbf{STATIC STATE-FEEDBACK CONTROL}}

    \blue{System Model} 
    {\footnotesize
        LTI, no noise, deterministic, perf. state info $z(k)=x(k)$ \\
    }
    
    \begin{whitebox}{}
        \vspace{-0.25em}
        \begin{align*}
            \begin{array}{c | c}
                \textbf{Feedback Law} & \textbf{CL Dynamics} \\
                u(k) = Fx(k) = Fz(k) &  x(k) = (A+BF)x(\kminone)
            \end{array}
        \end{align*}

        \vspace{0.25em}

        \begin{itemize}[leftmargin=1em]
            \item $(A+BF)$ must be stable (all $\lvert\lambda\rvert<1$)
            \item Such $F$ exists $\Leftrightarrow$ $(A,B)$ stabilizable
        \end{itemize}
    \end{whitebox}
    
    \blue{Controller Design}
    \begin{whitebox}{}
        \textbf{Pole Placement:} place (controllable) poles at des. CL loc. 

        \textbf{LQR:} find $F$ that min. quad. cost ($\bar{Q} = \bar{Q}^\top \geq 0, \bar{R} = \bar{R}^\top \geq 0$)
        \begin{align*}
            J_{\mathrm{LQR}} = \textstyle\sum_{k=0}^{\infty} x^\top(k)\bar{Q} x(k) + u^\top(k) \bar{R} u(k)
        \end{align*}
    \end{whitebox}

    \blue{LQR Design}
    \begin{highlightbox}{}
        \textbf{LQR Feedback Controller}
        {\footnotesize
            Assuming $(A,B)$ stabil., $(A,G)$ detect.
        }
        \begin{align*}
            F = -(B^\top P B + \bar{R})^{-1} B^\top P A
        \end{align*}

        \textbf{DARE} 
        {\footnotesize 
            Same as KF but $A\to A^\top, H\to B^\top, Q \to \bar{Q}, R\to \bar{R}$
        }
        \begin{align*}
            P = A^\top A + \bar{Q} - A^\top P B (B^\top P B + \bar{R})^{-1} B^\top P A
        \end{align*}
    \end{highlightbox}

\end{whitebox}

\begin{whitebox}{\textbf{SEPARATION PRINCIPLE}}
    \blue{Stable Observer + Controller} $\leadsto u(k) = F \widehat{x}(k)$ \\
    
    \begin{minipage}[c]{0.18\linewidth}
        \blue{Dynamics} w/o noise
    \end{minipage}
    \begin{minipage}[c]{0.80\linewidth}
        \mathboxplus{
            \begin{bmatrix}
                x(k) \\
                e(k)
            \end{bmatrix}
            =
            \begin{bmatrix}
                A+BF & -BF \\
                0 & (\mathbb{I}-KH)A
            \end{bmatrix}
            \begin{bmatrix}
                x(k-1) \\
                e(k-1)
            \end{bmatrix}
        }
    \end{minipage}

    \blue{Separation Principle}
    \begin{highlightbox}{}
        \begin{itemize}[leftmargin=1em]
            \item $(\mathbb{I}-KH)A$ and $(A+BF)$ stable $\leadsto$ \textbf{Overall system stable}
            \item Still stable with noise, Generalizable to time-varying case 
            \item Does \textbf{NOT} hold for nonlinear systems
        \end{itemize}
    \end{highlightbox}

    \blue{Separation Theorem} \textbf{Optimal} strategy for control problem 
    \begin{highlightbox}{}
        \begin{align*}
            J_{\mathrm{LQR}} = \lim_{N\to\infty} \mathbb{E}\left\{ \textstyle\frac{1}{N} \sum_{k=0}^{N-1}\left[ x^\top(k)\bar{Q}x(k) + u^\top(k)\bar{R}u(k)\right] \right\}
        \end{align*}
        \footnotesize
        \begin{enumerate}[leftmargin=1.5em]
            \item Design steady state KF to provide $\widehat{x}(k)$
            \textbf{(no dependency on $\boldsymbol{\bar{Q}, \bar{R}}$)}
            \item Design optimal state-FB law $u(k) = Fx(k)$ for determ. LQR problem \\
            minimizes $J_{\mathrm{LQR}}$ w/o noise \textbf{(no dependency on noise statistics $\boldsymbol{Q,R}$)}
            \item Profit.
        \end{enumerate}
    \end{highlightbox}
    
\end{whitebox}