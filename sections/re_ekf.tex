\section{EKF -- EXTENDED KALMAN FILTER}

\begin{whitebox}{\textbf{EKF -- MODELING}}

    \begin{minipage}[c]{0.55\linewidth}
        \blue{System} nonlinear, DT system 
        \mathbox{
            x(k) &= q_{k-1}(x(\kminone), v(\kminone)) \\
            z(k) &= h_k(x(k), w(k))
        }
        {\footnotesize
        \begin{itemize}[leftmargin=1.5em]
            \item $x(0), \{v\}, \{w\}$ mutually indep.
            \item $q_{k-1}, h_k$ assumed cont. diff'bar 
        \end{itemize}
        }
    \end{minipage}
    \begin{minipage}[c]{0.40\linewidth}
        \begin{align*}
            \begin{array}{l c c}
                \toprule
                & \mathbb{E}\{\cdot\} & \mathrm{Var}\{\cdot\} \\
                \midrule
                x(0) & x_0 & P_0 \\
                v(\kminone) & 0 & Q(\kminone) \\
                w(k) & 0 & R(k) \\
                \bottomrule
            \end{array}
        \end{align*}
    \end{minipage}

    \vspace{0.25em}

    {\footnotesize \blue{Distribution} not assumed GRV, but at least unimodal}
    
    \tcbsubtitle{\textbf{EKF DT EQUATIONS}}
    \blue{INITIALIZATION} $\quad\widehat{x}_m(0) = x_0, \quad P_m(0) = P_0$

    \blue{S1 -- Prior Update / Prediction Step} {\footnotesize\textbf{(Vec/Mat. at k-1 unless noted)}}
    \mathboxplus{
        \left.
        \begin{aligned}
            \widehat{x}_p(k) &= q_{k-1}(\widehat{x}_m(\kminone), 0) \\
            P_p(k) &= A P_mA^\top + L Q L^\top 
        \end{aligned}
        \
        \right\rvert
        \
        \begin{aligned}
            A(\kminone) &:= 
            \textstyle\frac{\partial q_{k-1}}{\partial x}(\widehat{x}_m(\kminone), 0) \\
            L(\kminone) &:= 
            \textstyle\frac{\partial q_{k-1}}{\partial v}(\widehat{x}_m(\kminone), 0)
        \end{aligned}
    }
    \blue{S2 -- A Posteriori / Measurement Update} {\footnotesize\textbf{(at k unless noted)}}
    \mathboxplus{
        \left.
        \begin{aligned}
            K &= P_p H^\top \!\left( H P_p H^\top
            \!+\! M R M^\top \right)^{-1} \\
            \widehat{x}_m &= \widehat{x}_p + K 
            \left( \bar{z} - h_k(\widehat{x}_p(k),0) \right) \\
            P_m &= \left(\mathbb{I} - K(k) H(k)\right) P_p(k)
        \end{aligned}
        \
        \right\rvert
        \
        \begin{aligned}
            H &:= 
            \textstyle\frac{\partial h_{k}}{\partial x}(\widehat{x}_p(k, 0)) \\
            M &:= 
            \textstyle\frac{\partial h_{k}}{\partial w}(\widehat{x}_p(k, 0))
        \end{aligned}
    }
    \blue{Intuition} \\ 
    {\footnotesize
    \textbf{Process Update} -- predict the mean state estimate fwd using NL process model and update the var. according to lin. eqns \\
    \textbf{Measurement Update} -- correct for mismatch between $\bar{z}(k)$
    and prediction $h_k(\widehat{x}_p(k),0)$, and correct var. according to lin. eqn \\
    }
    \blue{ACHTUNG}
    \begin{itemize}[leftmargin=1em]
        \item Process \& measurement noise \textbf{assumed} 0-mean
        \item $A,L,H,M$ lin. about current $\widehat{x}_m$ $\leadsto$ \textbf{cannot} compute offline
        \item Prior update only accurate if $\mathbb{E}\{\cdot\}$ \& $q_{k-1}(\cdot)$ commute
        \item \textbf{EKF Variables DO NOT capture true conditional mean \& var.}
    \end{itemize}
\end{whitebox}

\begin{whitebox}{\textbf{HYBRID EKF -- MODELING}}
    \blue{System} \textbf{Process} -- nlin, CT, \textbf{Measurement} -- nlin, DT
    \mathbox{
        \dot{x}(k) &= q(x(t), v(t), t) \\
        z[k] &= h_k(x[k], w[k]) \qquad \mathbb{E}\{w[k]\} = 0, \mathrm{Var}\{w[k]\} = R
    }
    
    \blue{CT White Noise} 
    \mathbox{
        \mathbb{E}\{v(t)\} = 0 \mathrm{ and } \mathbb{E}\{v(t)v^\top(t+\tau)\} = \mathcal{Q}_c \delta(\tau)
    }
    {\footnotesize
        \blue{ACHTUNG} \textbf{True CT White noise does not exist} $\leadsto$ Would require infinite power
    }

    \tcbsubtitle{\textbf{HYBRID EKF EQUATIONS}}

    \blue{INITIALIZATION} $\quad\widehat{x}_m(0) = x_0, \quad P_m(0) = P_0$
    
    \blue{S1 -- Prior Update / Prediction Step} Solve ODEs

    \begin{highlightbox}{}
        \footnotesize
        Solve, for $(\kminone)T \leq t \leq kT$ and $\hat{x}((\kminone)T) = \widehat{x}_m[\kminone]$
        \begin{align*}
            \dot{\hat{x}}(t) = q(\hat{x}(t),0,t)
            \quad \Rightarrow \hat{x}_p[k] := \hat{x}(kT)
        \end{align*}
        Solve, for $(\kminone)T \leq t \leq kT$ and $P((\kminone)T) = P_m[\kminone]$
        \begin{align*}
            \dot{P}(t) = A P \!+\! PA^\top \!+\! LQ_c L^\top 
            \quad \Rightarrow P_p[k] := P(kT)
        \end{align*}
        with
        \begin{align*}
            A(t) := 
            \textstyle\frac{\partial q}{\partial x}(\hat{x}(t),0,t),
            \qquad L(t) := 
            \textstyle\frac{\partial q}{\partial v}(\hat{x}(t),0,t)
        \end{align*}
    \end{highlightbox}
    \blue{S2 -- A Posteriori Update / Measurement Update} \\
    \begin{highlightbox}{}
        \centering
        \textbf{Measurement model still DT -- S2 identical to standard EKF}
    \end{highlightbox}
    \blue{ACHTUNG}
    \begin{itemize}[leftmargin=1em]
        \item Process \& measurement noise \textbf{assumed} 0-mean
        \item ODEs typically solved num. Accuracy $\uparrow$ $\leadsto$ Comp. Cost $\uparrow$
    \end{itemize}
\end{whitebox}