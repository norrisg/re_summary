\section{PF -- PARTICLE FILTER}

\begin{whitebox}{\textbf{MCS -- MONTE CARLO SAMPLING -- DRV}}

    \blue{Samples} $N$ DRVs $y^n$ of samples of $y\in\mathcal{Y}=\{1,\dots,\bar{Y}\}$
    
    \blue{Helper Variables}
    \vspace{-1em}
    \begin{align*}
        \left.
        s_i^n = \delta(i-y^n) = 
        \begin{cases}
            1 & \mathrm{if } y^n=i \\
            0 & \mathrm{otherwise}
        \end{cases}
        \ \right\rvert \
        \begin{aligned}
            i &= 1,\dots, \bar{Y} \\
            n &= 1,\dots, N
        \end{aligned}
    \end{align*}

    \blue{Average} -- $s_i$ converges to $p_y(i)$ by Law of Large Numbers
    \begin{align*}
        \textstyle\frac{1}{N} \sum_{n=1}^{N} s_i^n 
        \xrightarrow{N\to\infty} \mathbb{E}\{s_i^n\} = p_y(i)
        \qquad 
        p_y(i) \approx \textstyle\frac{1}{N} \sum_{n=1}^{N}\bar{s}_i^n
    \end{align*}
    
    \blue{Approximation}
    \begin{highlightbox}{}
        For $y\in\mathcal{Y}$, it holds 
        $\quad p_y(i) \approx \textstyle\frac{1}{N}\sum_{n=1}^{N}
        \delta(i-\bar{y}^n), 
        \quad i \in \mathcal{Y}$

        For $x=g(y)$, with $x\in\mathcal{X} := g(\mathcal{Y})$, it holds
        \begin{align*}
            p_x(j) \approx \textstyle\frac{1}{N}\sum_{n=1}^{N}
            \delta(j-g(\bar{y}^n)), 
            \quad j \in g(\mathcal{Y})
        \end{align*}
    \end{highlightbox}
    
    \tcbsubtitle{\textbf{MCS -- CRV}}
    \blue{Helper Variables}
    \vspace{-1em}
    \begin{align*}
        s_a^n = \textstyle\int_{a}^{a+\Delta y} \delta(i-y^n) d\xi = 
        \begin{cases}
            1 & \mathrm{if } a \leq y^n < a + \Delta y \\
            0 & \mathrm{otherwise}
        \end{cases}
    \end{align*}

    \blue{Average} -- $s_a$ converges to $p_y(\xi)$ by Law of Large Numbers
    \begin{align*}
        \textstyle\int_{a}^{a+\Delta y}\frac{1}{N} \sum_{n=1}^{N} \delta(\xi-y^n)d\xi
        \xrightarrow{N\to\infty}
        \textstyle\int_{a}^{a+\Delta y} p_y(\xi)d\xi
    \end{align*}
    \blue{Approximation}
    \begin{highlightbox}{}
        \begin{align*}
            \begin{array}{l l l}
                \mathrm{For } y\in\mathcal{Y} \mathrm{, it holds} & 
                p_y(\xi) \approx \textstyle\frac{1}{N}\sum_{n=1}^{N}\delta(\xi-\bar{y}^n) & \forall\xi \\
                \mathrm{For } x=g(y) \mathrm{, it holds} &
                p_x(\xi) \approx \textstyle\frac{1}{N}\sum_{n=1}^{N}\delta(\xi-g(\bar{y}^n)) & \forall\xi
            \end{array}
        \end{align*}
    \end{highlightbox} 
\end{whitebox}

\begin{whitebox}{\textbf{PF -- MODELING}}
    \begin{minipage}[c]{0.45\linewidth}
        \blue{System} \textbf{NL, DT}
        {\footnotesize
        \begin{itemize}[leftmargin=1.5em]
            \item $x(0), \{v\}, \{w\}$ mutually indep.
            \item can be D- or CRVs, known PDFs
        \end{itemize}
        }
    \end{minipage}
    \begin{minipage}[c]{0.53\linewidth}
        \mathbox{
            x(k) &= q_{k-1}(x(\kminone), v(\kminone)) \\
            z(k) &= h_k(x(k), w(k))
        }
    \end{minipage}

    \vspace{0.25em}
    
    \blue{Objective} \textbf{Approximate} Bayesian State Estimator with MCS \\

    \begin{minipage}[c]{0.20\linewidth}
        \blue{Auxiliary Variables} \\
        Same as KF
    \end{minipage}
    \begin{minipage}[c]{0.78\linewidth}
        \footnotesize
        \mathbox{
            \begin{array}{l r l}
                \textbf{Init:} &x_m(0) &:= x(0) \\
                \textbf{S1:} &x_p(k) &:= q_{k-1}(x_m(\kminone), v(\kminone)) \\
                \textbf{S2:} &z_m(k) &:= h_k(x_p(k), w(k)) \\
                &p_{x_m(k)}(\xi) &:= p_{x_p(k)|z_m(k)}(\xi | \bar{z}(k)) \quad \forall\xi
            \end{array}
        }
    \end{minipage}

    \vspace{0.25em}

    Similar proof for $p_{x_p(k)}(\xi), p_{x_m(k)}(\xi) \forall\xi$ as for KF aux. var.
\end{whitebox}

\begin{whitebox}{\textbf{PF -- EQUATIONS}}
    \blue{Initialization} Draw $N$ samples $\{ \bar{x}_m^n(0) \}$
    from $p_{x(0)}$ \\
    \blue{S1 -- Prior Update / Prediction Step}
    \begin{highlightbox}{}
        Apply process equation to the particles $\{ \bar{x}_m^n(k-1) \}$
        \begin{align*}
            \bar{x}_p^n(k) := q_{k-1}(\bar{x}_m^n(k-1), \bar{v}^n(k-1)), 
            \qquad n=1,\dots,N
        \end{align*}
        Requires $N$ noise samples from $p_{v(k-1)}$
    \end{highlightbox}
    \blue{S2 -- A Posteriori Update / Measurement Update}
    \begin{highlightbox}{}
        Scale each particle by measurement likelihood:
        \begin{align*}
            \beta_n = \alpha p_{z(k)|x(k)}(\bar{z}(k)|\bar{x}_p^n(k)),
            \qquad n=1,\dots,N
        \end{align*}
        with $\alpha$ normalization s.t $\sum_{n=1}^N \beta_n = 1$
        \begin{align*}
            \alpha = \left( \textstyle\sum_{n=1}^{N} p_{z_m(k)|x_p(k)}(\bar{z}(k) | \bar{x}_p^n(k)) \right)^{-1}
        \end{align*}
    \end{highlightbox}
    \blue{Resample} to get $N$ posterior particles
    $\{ \bar{x}_m^n(k) \}$, w/ eq. weights
    \begin{highlightbox}{}
        Repeat $N$ times:
        \begin{itemize}[leftmargin=1em]
            \item Select random number $r$ uniformly on $(0,1)$
            \item Pick particle $\bar{n}$ s.t $\sum_{n=1}^{\bar{n}-1}\beta_n < r \leq \sum_{n=1}^{\bar{n}}\beta_n$
        \end{itemize}
        New particles all have equal weight
        \begin{align*}
            p_{x_m(k)}(\xi) \approx \textstyle\frac{1}{N}\sum_{n=1}^{N}\delta(\xi - \bar{x}_m^n(k)), \quad \forall\xi
        \end{align*}
    \end{highlightbox}

    \blue{Intuition} \\
    {\footnotesize
    \textbf{Process Update} -- Propagate $N$ particles through system dynamics. 
    Provided $N$ large, $\{ \bar{x_m^n(k-1)} \} \approx p_{x_m(k-1)}$ $\leadsto$ $\{\bar{x}_p^n(k)\} \approx p_{x_p(k)}$\\
    \textbf{Measurement Update} -- treat each particle separately. 
    At points of high prior, we have many particles. 
    Posterior has same particles, but scaled by measurement likelihood.
    }
\end{whitebox}
\begin{whitebox}{\textbf{ROUGHENING}}
    \blue{Sample Impoverishment} \textbf{Lumpiness} -- when we resample, only retain subset of particles. For finite $N$ this is problem. \\
    \blue{Roughening} 
    \begin{highlightbox}{}
        Perturb particles \textbf{after} resampling 
        \begin{align*}
            \bar{x}_m^n(k) \leftarrow \bar{x}_m^n (k) + \Delta x^n(k)
        \end{align*}
        $\Delta x^n(k)$ drawn from 0-mean, finite-var. distribution \\
        \textbf{Example Method for $\boldsymbol{\Delta x^n}$}: $\sigma_i$ stand. dev. of $\Delta x_i^n(k)$
        \begin{align*}
            \sigma_i = K E_i N^{-\frac{1}{d}}
        \end{align*}
        {\footnotesize
        \begin{align*}
            \begin{array}{l l l l}
                K: & \mathrm{tuning param. } K \ll 1 &
                d: & \mathrm{dim. of state space} \\
                E_i: & \mathrm{max inter-sample variablility} & N^{-\frac{1}{d}}: & \mathrm{node spacing on grid}\\
                & \max_{n_1, n_2} \lvert \bar{x}_{m,i}^{n_1} - \bar{x}_{m,i}^{n_2} \rvert
            \end{array}
        \end{align*}
        }
    \end{highlightbox}
    
\end{whitebox}