\section{KALMAN FILTER}

\begin{whitebox}{\textbf{MODEL}}
    \begin{minipage}[c]{0.70\linewidth}
        \blue{Linear Time-Varying System}
        \mathbox{
            x(k) & = A(\kminone) x(\kminone) + u(\kminone) + v(\kminone) \\
            z(k) &= H(k) x(k) + w(k)
        }
    \end{minipage}
    \begin{minipage}[c]{0.28\linewidth}
        \footnotesize
        \begin{itemize}[leftmargin=1em]
            \item $x(0), v(\cdot), w(\cdot)$ ind.
            \item $x(0) \sim \mathcal{N}(x_0, P_0)$
            \item $v \sim \mathcal{N}(0, Q(k))$
            \item $w \sim \mathcal{N}(0, R(k))$
        \end{itemize}
    \end{minipage}
\end{whitebox}

\begin{whitebox}{\textbf{GRV -- GAUSSIAN RANDOM VARIABLE}}
    \blue{PDF} Gaussian distributed vector CRV $y = (y_1, \dots, y_D)$
    \mathboxplus{
        p(y) = \frac{1}{(2\pi)^{D/2} \det(\Sigma)^{1/2}}
        \exp\left\{ -\textstyle\frac{1}{2}(y-\mu)^\top \Sigma^{-1} (y-\mu) \right\}
    }
    $\leadsto$ fully characterized by mean $\mu \in \mathbb{R}^D$ and var $\Sigma \in \mathbb{R}^{D\times D}$

    \tcbline

    \begin{minipage}[c]{0.45\linewidth}
        \blue{Diagonal Variance} with 
        $\Sigma = \left[\begin{smallmatrix}
            \sigma_1^2 & \\
            & \sigma_D^2
        \end{smallmatrix}\right]$, 
        $\mu = \left[\begin{smallmatrix}
            \mu_1 \\
            \mu_D
        \end{smallmatrix}\right]$ \\
        $\leadsto \ y_i$ mut.indep. iff $\Sigma$ diag.
    \end{minipage}
    \begin{minipage}[c]{0.5\linewidth}
        \mathbox{
            p(y) = \prod_{i=1}^{D} \textstyle\frac{1}{\sqrt{2\pi\sigma_i^2}} 
            e^{-\frac{(y_i - \mu_i)^2}{2\sigma_i^2}}
        }
    \end{minipage}
    
    \tcbsubtitle{\textbf{JGRV -- JOINTLY GAUSSIAN RANDOM VARIABLES}}

    \blue{Jointly GRV} if joint vector RV $(x,y)$ a GRV

    \begin{highlightbox}{}
    \textbf{Property 1}\textbf{ -- Affine Transformation of GRV is GRV} \\
    $y$ a GRV $\leadsto x = My + b$ (M,b constant) $\leadsto x$ also GRV \\
    \textbf{Property 2}\textbf{ -- Linear Comb. of 2 Jointly GRV is GRV} \\
    $x,y$ JGRV, $\leadsto z=M_x x + M_y y$ ($M_x, M_y$ constant matrices) 
    $\leadsto z$ is a GRV $z \sim \mathcal{N}(\mu_z, \Sigma_z)$
    \end{highlightbox}
    \blue{ACHTUNG} $x\sim \mathcal{N}(\mu_x, \Sigma_x)$, $y\sim \mathcal{N}(\mu_y, \Sigma_y)$ does not imply $(x,y)$ jointly GRV 
        $\leadsto$ \textbf{Must be additionally independent} 
\end{whitebox}

\begin{whitebox}{\textbf{AUXILIARY VARIABLES}}
    \blue{Problem Formulation} want $p_{x(k)|z(1:k)} \leadsto$ bayesian tracking provides this
    \textbf{BUT} we have \textbf{CRVs} and don't want integrals \\
    $\Rightarrow$ exploit linearity, GRV to convert to matrix manipulations

    \tcbline 

    \blue{Auxilliary Variables}
    \mathboxplus{
        \begin{array}{l r l}
            \textbf{Init:} &x_m(0) &= x(0) \\
            \textbf{S1:} &x_p(k) &= A(\kminone)x_m(\kminone) + u(\kminone) + v(\kminone) \\
            \textbf{S2:} &z_m(k) &= H(k) x_p(k) + w(k) \\
            &p_{x_m(k)}(\xi) &:= p_{x_p(k)|z_m(k)}(\xi | \bar{z}(k)) \quad \forall\xi
        \end{array}
    }
    
    \begin{minipage}[c]{0.3\linewidth}
        \blue{Equivalency} $x$ conditioned on $z(\onetokminone)$/$z(\onetok)$
    \end{minipage}
    \begin{minipage}[c]{0.65\linewidth}
        \mathbox{
            p_{x_p(k)}(\xi) &= p_{x(k)|z(1:k-1)}(\xi|\bar{z}(\onetokminone)) \\
            p_{x_m(k)}(\xi) &= p_{x(k)|z(1:k)}(\xi|\bar{z}(\onetok))
        }
    \end{minipage}
    
    \vspace{0.25em}

    \blue{GRV} $x_p$ and $x_m$ are GRVs for $k=1,2,\dots$

    \begin{minipage}[c]{0.45\linewidth}
        \mathbox{
            \widehat{x}_p(k) &:= \mathbb{E}\left\{ x_p(k) \right\} \\
            \widehat{x}_m(k) &:= \mathbb{E}\left\{ x_m(k) \right\}
        }
    \end{minipage}
    \begin{minipage}[c]{0.45\linewidth}
        \mathbox{
            P_p(k) &:= \mathrm{Var}\left\{ x_p(k) \right\} \\
            P_m(k) &:= \mathrm{Var}\left\{ x_m(k) \right\}
        }
    \end{minipage}
\end{whitebox}

\begin{whitebox}{\textbf{KF -- KALMAN FILTER}}

    \blue{Recursive Update Equations}
    \begin{highlightbox}{}
        \textbf{Initialization} $\quad\widehat{x}_m(0) = x_0, \quad P_m(0) = P_0$ \\
        \textbf{S1 -- Prior Update / Prediction Step}
        \begin{align*}
            \widehat{x}_p(k) &= A(k-1) \widehat{x}_m(k-1) + u(k-1) \\
            P_p(k) &= A(k-1)P_m(k-1)A^\top(k-1) + Q(k-1)
        \end{align*}
        \textbf{S2 -- A Posteriori Update / Measurement Update}
        \begin{align*}
            P_m(k) &= \left( P_p^{-1}(k) + H^\top(k) R^{-1}(k) H(k) \right)^{-1}\\
            \widehat{x}_m(k) &= \widehat{x}_p(k) + 
            \underbrace{P_m(k) H^\top(k) R^{-1}(k)}_{\mathrm{correction factor}}
            \underbrace{\left( \bar{z}(k) - H(k) \widehat{x}_p(k) \right)}_{\mathrm{prediction error}}
        \end{align*}
    \end{highlightbox}

    \blue{Alternative Equations} \textbf{(all matrices/vectors at time k)}
    \begin{highlightbox}{}
        \textbf{S2 -- Kalman Filter Gain}
        \begin{align*}
            K &= P_p H^\top \left( H P_p H^\top \!+\! R \right)^{-1} \\
            \widehat{x}_m &= \widehat{x}_p + K \left( \bar{z} \!-\! H \widehat{x}_p \right) \\
            P_m &= \underbrace{\left( \mathbb{I} - K H \right) P_p}_{\mathrm{cheaper}}
        = \underbrace{\left( \mathbb{I} - K H \right) P_p \left( \mathbb{I} - K H \right)^\top + K R K^\top}_{\mathrm{fewer numerical errors}}
        \end{align*}
    \end{highlightbox}

    \blue{ACHTUNG} \textbf{EQUIVALENT TO RLS} $\leadsto$ among linear, unbiased estimators KF minimizes MSE
\end{whitebox}

\begin{whitebox}{\textbf{KF REMARKS}}
    \blue{Known Data} $A(k),H(k),Q(k),R(k) \ \forall k$ as well as $P_0$ $\leadsto$ KF matrices $P_p(k), P_m(k), K(k)$ can be computed offline
    \mathbox{
        &\widehat{x}_p(k) = A(k-1) \widehat{x}_m(k-1) + u(k-1) \\
        &\widehat{x}_m(k) = \\
        &\underbrace{(\mathbb{I} - KH) A(\kminone)}_{\tilde{A}(k)}
        \widehat{x}_m(\kminone)
        + \underbrace{(\mathbb{I} - KH)}_{\tilde{B}(k)} u(\kminone) + K\bar{z}(k)
    }
    \blue{ACHTUNG} \textbf{Filter is linear, time-varying}

    \tcbline

    \blue{Nonlinear Systems} \textbf{KF not guaranteed to be optimal} -- non-linear estimators may do better
\end{whitebox}

\vfill\null
\columnbreak

\begin{whitebox}{\textbf{ASYMPTOTIC PROPERTIES OF KF}}
    \blue{Model} assume \textbf{time-invariant System, stationary Distributions} ($A, H, Q, R$ constants) \\
    \blue{Change of Variance}
    \mathbox{
        P_p({\scriptstyle k+1}) &= \left(A  P_p({\scriptstyle k}) A^\top\right) + Q \\
        &- \left( A P_p({\scriptstyle k}) H^\top \right) 
        \left( H  P_p({\scriptstyle k}) H^\top \!\!+\! R \right)^{-1} 
        \left( H P_p({\scriptstyle k}) A^\top\right)
    }

    \blue{Scalar Case} $P_p(k+1) = \frac{a^2 r P_p(k)}{h^2 P_p(k) + r}+q =: f(P_p(k))$ \\

    \blue{Summary} 
    \begin{highlightbox}{}
        \textbf{Variance Converges to unique steady-state sol'n \\
        provided either $\lvert a\rvert <1$ or if $\lvert a \rvert \geq 1$ $\Rightarrow$ 
        $h \neq 0$ and $q >0$}
    \end{highlightbox}
\end{whitebox}

\begin{whitebox}{\textbf{DETECTABILITY / STABILIZABILITY}}
    \blue{Detectability Conditions}
    Pair $(A,H)$ detectable
    \begin{highlightbox}{}
        \begin{itemize}[label=$\Leftrightarrow$, leftmargin=1.6em]
            \item For deterministic LTI Sys. ($\scriptstyle x(k) = Ax(k-1), z(k) = Hx(k)$) \\
            $\lim_{k\to\infty}z(k)=0 \Rightarrow \lim_{k\to\infty}x(k)=0 \
            \quad \forall x_0 \in \mathbb{R}^n$
            \item $\left[\begin{smallmatrix}
                A - \lambda \mathbb{I} \\
                H
            \end{smallmatrix}\right]$ is full rank for all $\lambda \in \mathbb{C}$ 
            with $\lvert \lambda\rvert \geq 1$ (PBH-Test)
            \item Eigenvalues of $A-LH$ (or $(\mathbb{I}-LH)A$) can be placed within unit circle
            by suitable choice of $L\in\mathbb{R}^{n\times m}$
        \end{itemize}
    \end{highlightbox}
    \blue{Stabilizability Conditions}
    Pair $(A,B)$ stabilizable
    \begin{highlightbox}{}
        \begin{itemize}[label=$\Leftrightarrow$, leftmargin=1.6em]
            \item For deterministic LTI Sys. ($\scriptstyle x(k) = Ax(k-1) + Bu(k-1)$) \\
            $\exists \ u(0:k-1) \mathrm{ s.t. } \lim_{k\to\infty}x(k)=0 
            \quad \forall x_0 \in \mathbb{R}^n$
            \item $\left[\begin{smallmatrix}
                A - \lambda \mathbb{I} & B
            \end{smallmatrix}\right]$ full rank $\forall \lambda \in \mathbb{C}$ 
            with $\lvert \lambda\rvert \geq 1$ (PBH-Test)
            \item Eigenvalues of $A-BK$ (or $(\mathbb{I}-BK)A$) can be placed within unit circle
            by suitable choice of $K\in\mathbb{R}^{m\times n}$
        \end{itemize}
    \end{highlightbox}
    \blue{NOTE} Stabilizability is dual of detectability, $(A,B)$ stabilizable 
    if $(A^\top, B^\top)$ detectable 

    {\footnotesize
        \begin{minipage}[c]{0.30\linewidth}
            \blue{Observability Matrix}  implies detectability
        \end{minipage}
        \begin{minipage}[c]{0.15\linewidth}
            $\left[\begin{smallmatrix}
                H \\
                HA^{n-1}
            \end{smallmatrix}\right]$
        \end{minipage}
        \begin{minipage}[c]{0.30\linewidth}
            \blue{Reachability Matrix}  implies stabilizablilty
        \end{minipage}
        \begin{minipage}[c]{0.15\linewidth}
            $\left[\begin{smallmatrix}
                A &
                A^{n-1}B
            \end{smallmatrix}\right]$
        \end{minipage}
    }
\end{whitebox}

\begin{whitebox}{\textbf{STEADY-STATE KF}}
    \blue{DARE -- Discrete Algebraic Ricatti Equation}
    \mathboxplus{
        P_\infty = AP_\infty A^\top + Q 
        - AP_\infty H^\top 
        (H P_\infty H^\top + R)^{-1}
        H P_\infty A^\top
    }

    \begin{minipage}[c]{0.40\linewidth}
        \blue{Steady-State KF Gain} 
    \end{minipage}
    \begin{minipage}[c]{0.58\linewidth}
        \mathboxplus{
        K_\infty = P_\infty H^\top (H P_\infty H^\top \!\!+\! R)^{-1}
    }
    \end{minipage}

    \vspace{0.25em}

    \blue{Filter Equations} \textbf{Linear Time-Invariant System}
    \mathboxplus{
        \widehat{x}(k) = 
        \underbrace{(\mathbb{I} \!-\! K_\infty H) A}_{\widehat{A}} 
        \widehat{x}({\scriptstyle k - 1}) + 
        \underbrace{(\mathbb{I} \!-\! K_\infty H)}_{\widehat{B}}
        u({\scriptstyle k - 1}) \!+\! K_\infty \bar{z}(k)
    }

    \tcbline

    \blue{Error} $e(k) = x(k) - x_f(k) \equiv \mathrm{true} - \widehat{x}$ with $z$ as RV 
    \mathbox{
        &e(k) = (\mathbb{I} \!-\! K_\infty H)A e(\kminone) 
        + (\mathbb{I} \!-\! K_\infty H) v(\kminone) - K_\infty w({\scriptstyle k}) \\
        &\mathbb{E}\{e(k)\} = (\mathbb{I} \!-\! K_\infty H)A \mathbb{E}\{e(\kminone)\}
    }
    \begin{itemize}[leftmargin=1em]
        \item $(\mathbb{I}-K_\infty H)A$ must be stable (all $\lvert\lambda\rvert < 1$) for $e$ not to diverge
        \item $\mathbb{E}\{e(k)\} \xrightarrow{k\to\infty} 0$ iff $(\mathbb{I}-K_\infty H)A$ stable
    \end{itemize}

    \tcbline

    \blue{Theorem} for $R>0, Q \geq 0, G \mathrm{ s.t. } Q=GG^\top$
    \begin{highlightbox}{}
        \begin{itemize}[label=$\Leftrightarrow$, leftmargin=1.6em]
            \item $(A,H)$ is detectable, $(A,G)$ is stabilizable
            \item DARE has unique pos. semidef. sol. $P_\infty \geq 0$, 
            resulting $(\mathbb{I} - K_\infty H)A$ is stable and
            \begin{align*}
                \lim_{k\to\infty} P_p(k) = P_\infty \quad \forall P_p(1) \geq 0
            \end{align*}
        \end{itemize}
    \end{highlightbox}
    \begin{description}
        \item[(A,H) Detectable:] can observe all unstable modes 
        \item[(A,G) Stabilizable:] noise excites unstable modes
    \end{description}
    \blue{Note} if $Q>0 \Rightarrow (A,G)$ always stabilizable
\end{whitebox}