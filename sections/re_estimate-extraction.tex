\section{ESTIMATE EXTRACTION}

\begin{whitebox}{\textbf{GENERAL MEASUREMMENT}}
    \begin{minipage}[c]{0.25\linewidth}
        \blue{General \break Measurement}
        \begin{itemize}[leftmargin=1em]
            \item m measure.
            \item n states
        \end{itemize}
    \end{minipage}
    \begin{minipage}[c]{0.70\linewidth}
        \mathboxplus{
            \underbrace{
                \begin{bmatrix}
                    z_1 \\
                    z_m
                \end{bmatrix}
            }_{z \ \in \ \mathbb{R}^m}
            = 
            \underbrace{
                \begin{bmatrix}
                    h_{11} & h_{1n} \\
                    h_{m1} & h_{mn}
                \end{bmatrix}
            }_{H \ \in \ \mathbb{R}^{m\times n}}
            \cdot
            \underbrace{
                \begin{bmatrix}
                    x_1 \\
                    x_n
                \end{bmatrix}
            }_{x \ \in \ \mathbb{R}^n}
            +
            \underbrace{
                \begin{bmatrix}
                    w_1 \\
                    w_m
                \end{bmatrix}
            }_{w \ \in \ \mathbb{R}^m}
        }
    \end{minipage}
    
    \blue{Noise} $w_i \sim \mathcal{N}(0,1)$ -- Zero-mean additive gaussian \\
    \blue{Independence} $x,w_i \ \forall i$ assumed mutually independent
\end{whitebox}

\begin{whitebox}{\textbf{ML -- MAXIMUM LIKELIHOOD}}
    \blue{Interpretation} Choice of $x$ to make $\bar{z}$ most likely
    \mathboxplus{
        \begin{drcases}
            x\in\mathcal{X} & \mathrm{unknown} \\
            p_x(\bar{x}) & \mathrm{unknown}
        \end{drcases}
        \quad
        \widehat{x}^{ML} := \mathop{\mathrm{argmax}}_{\bar{x}\in\mathcal{X}}
        \left[ p_{z|x}(\bar{z}|\bar{x}) \right]
    }
    
    \blue{ACHTUNG} $p_{z|x}(\bar{z}|\bar{x})$ is var. --  technically not a PDF

    \tcbsubtitle{\textbf{MAP -- MAXIMUM A POSTERIORI}}
    \blue{Interpretation} ML given observation \& prior belief about $x$
    \mathboxplus{
        \begin{drcases}
            x\in\mathcal{X} & \mathrm{unknown} \\
            p_x(\bar{x}) & \textbf{known}
        \end{drcases}
        \quad
        \widehat{x}^{MAP} := \mathop{\mathrm{argmax}}_{\bar{x}\in\mathcal{X}}
        \left[ p_{z|x}(\bar{z}|\bar{x}) p_x(\bar{x}) \right]
    }

    \blue{Comparison} $\widehat{x}^{MAP} = \widehat{x}^{ML}$ for constant $p_x(\bar{x})$
\end{whitebox}

\begin{whitebox}{\textbf{LS -- LEAST SQUARES}}
    \blue{Interpretation} ML estimate when errors are indep. 0-mean, same variance, $\mathcal{N}$-distributed
    \mathboxplus{
        \widehat{x}^{LS} := \mathop{\mathrm{argmin}}_{x}
        \left[(z-Hx)^\top (z-Hx)\right] = (H^\top H)^{-1} H^\top z
    }
    \blue{Comparison} $\widehat{x}^{LS} = \widehat{x}^{ML}$ for gaussian noise
    \tcbsubtitle{\textbf{WLS -- WEIGHTED LEAST SQUARES}}
    \blue{Interpretation} Given $1\!:\!k$, e-terms with var$\downarrow$ weighed $\uparrow$
    \mathboxplus{
        \widehat{x}^{WLS} 
        :=& \textstyle\mathop{\mathrm{argmin}}_{x} 
        \left[ (\bar{z} \!-\! H \widehat{x})^\top 
        R^{-1} (\bar{z} \!-\! H \widehat{x}) \right] \\
        =& (H^\top R H)^{-1} H^\top R^{-1} \bar{z}
    }
    \blue{Incorporate Prior Knowledge on x}
    \begin{description}
        \item[Define:] $r\sim\mathcal{N}(0,P_x) \Rightarrow x := \widehat{x} + r, \mathbb{E}\{x\} = \widehat{x}_0, \mathrm{Var}\{x\} = P_x$ 
        \item[Introduce Extended System:]
    \end{description}
    \begin{align*}
        \tilde{\bar{z}} = \left[ \begin{smallmatrix}
            \widehat{x}_0 \\
            z(1:k)
        \end{smallmatrix} \right],
        \tilde{H} = \left[ \begin{smallmatrix}
            \mathbb{I} \\
            H(1:k)
        \end{smallmatrix} \right],
        \tilde{w} = \left[ \begin{smallmatrix}
            -r \\
            w(1:k)
        \end{smallmatrix} \right],
        \tilde{R}= \left[ \begin{smallmatrix}
            P_x & 0 \\
            0 & R(1:k)
        \end{smallmatrix} \right]
    \end{align*}
    
    \tcbsubtitle{\textbf{RLS -- RECURSIVE LEAST SQUARES}}
    \blue{Interpretation} Update $\widehat{x}$ recursively when new $\bar{z}(k)$ arrives

    \vspace{0.25em}

    \begin{minipage}[c]{0.30\linewidth}
        \blue{Estimation Error} \textbf{(Matrices at k)}
    \end{minipage}
    \begin{minipage}[c]{0.65\linewidth}
        \mathbox{
            e(k) &= \left( \mathbb{I} - KH \right) e(k-1) - Kw(k) 
        }
    \end{minipage}

    \vspace{0.25em}

    \blue{Objective} Minimize MSE with $P(k) = \mathrm{Var} \left\{ e(k) \right\}$
    \mathbox{
        J(k):= \mathbb{E}\left\{ e^\top(k)e(k) \right\} 
        = \mathbb{E}\left\{ \mathrm{trace}(P(k)) \right\}
    }

    \blue{Algorithm}
    \begin{highlightbox}{}
        \begin{description}
            \item[Initialization:] $\widehat{x}(0) = \widehat{x}_0, \quad  P(0) = P_x = \mathrm{Var}\{x\}$
            \item[Recursion:] Observe $\bar{z}(k)$, then update:
            \begin{align*}
                K(k) &= P(\kminone)H^\top\left( H P(\kminone) H^\top + R \right)^{-1} \\
                \widehat{x}(k) &= \widehat{x}(\kminone) + K
                \left( \bar{z} - H \widehat{x}(\kminone) \right) \\
                P(k) &= \left(\mathbb{I} - KH\right)P(\kminone) \left(\mathbb{I} - KH\right)^\top + KRK^\top
            \end{align*}
        \end{description}
    \end{highlightbox}
\end{whitebox}